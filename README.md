# Assignment ALDA2 
## Setting up a local development environment
### Requirements

- Java 8

### Run Project
* `./gradlew bootRun` in Terminal  
or
* run the "run" goal in IntelliJ

You can access the website at http://localhost:8080.

object Deps {

    //Plugins
    const val plugin_spring_boot_version = "2.4.1"
    const val plugin_spring_deps_management_version = "1.0.10.RELEASE"

    //etc
    const val lombok_version = "1.18.16"
    const val retrofit2_version = "2.9.0"
    const val commons_io_version = "2.6"
    const val lucene_version = "8.4.1"

}

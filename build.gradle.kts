import org.gradle.plugins.ide.idea.model.IdeaModel

group = "ie.trinity"
version = "0.0.1"

plugins {
    java
    id("idea")
    id("org.springframework.boot") version Deps.plugin_spring_boot_version
    id("io.spring.dependency-management") version Deps.plugin_spring_deps_management_version
}

configure<IdeaModel> {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

dependencies {
    // spring
    implementation( group = "org.springframework.boot", name = "spring-boot-starter-web")
    implementation( group = "org.springframework.boot", name = "spring-boot-devtools")
    implementation( group = "org.springframework.boot", name = "spring-boot-starter-thymeleaf")

    // lombok
    implementation(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)
    testImplementation(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)
    annotationProcessor(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)
    testAnnotationProcessor(group = "org.projectlombok", name = "lombok", version = Deps.lombok_version)

    // utils
    implementation(group = "commons-io", name = "commons-io", version = Deps.commons_io_version)
    implementation(group = "org.apache.lucene", name = "lucene-spatial", version = Deps.lucene_version)
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

configure<JavaPluginExtension> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

configure<IdeaModel> {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

tasks {
    // Use the built-in JUnit support of Gradle.
    "test"(Test::class) {
        useJUnitPlatform()
    }
}

package ie.trinity.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transfer {

    private Integer fromStopId;

    private Integer toStopId;

    private Short transferType;

    private Integer minTransferTime;

    public double getCost() {
        if (this.transferType == 0) return 2.;
        return this.minTransferTime / 100.;
    }
}

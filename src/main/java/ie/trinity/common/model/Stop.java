package ie.trinity.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.util.SloppyMath;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stop {

    private Integer id;

    private String zoneId;

    private Integer code;

    private String name;

    private String description;

    private Double lat;

    private Double lon;

    private String url;

    private String locationType;

    private String parentStation;

    public Double calculateDistance(Stop stop) {
        return SloppyMath.haversinMeters(this.lat, this.lon, stop.getLat(), stop.getLon());
    }

}

package ie.trinity.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StopTime {

    private Integer tripId;

    private Integer stopId;

    private LocalTime arrivalTime;

    private LocalTime departureTime;

    private Integer stopSequence;

    private String headSign;

    private Short pickUpType;

    private Short dropOffType;

    private Double shapeDistanceTraveled = 0.;

    public double getCost() {
        return 1.;
    }
}

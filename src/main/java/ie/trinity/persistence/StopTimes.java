package ie.trinity.persistence;

import ie.trinity.common.model.StopTime;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StopTimes {
    private final Map<LocalTime, List<StopTime>> stopTimes = new HashMap<>();

    public void put(LocalTime time, StopTime stopTime) {
        List<StopTime> list = stopTimes.getOrDefault(time, new ArrayList<>());
        list.add(stopTime);
        stopTimes.putIfAbsent(time, list);
    }

    public List<StopTime> get(LocalTime time) {
        return this.stopTimes.getOrDefault(time, new ArrayList<>());
    }
}

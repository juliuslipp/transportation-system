package ie.trinity.persistence.data_structures;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TernarySearchTree<T> {

    private Node<T> root;

    public void put(String key, T value) {
        if (key == null) return;
        if (root == null) {
            root = new Node<>(key.toLowerCase(), value);
        } else {
            root.put(key.toLowerCase(), value);
        }
    }

    public List<T> search(String query) {
        if (root == null) return new ArrayList<>();
        ArrayList<Node<T>> result = new ArrayList<>();
        root.search(query != null ? query.toLowerCase() : null, result, 0);
        return result.stream().map((node) -> node.value).collect(Collectors.toList());
    }

    public static class Node<T> {
        private final Character character;
        private T value;

        private Node<T> left;
        private Node<T> mid;
        private Node<T> right;

        public Node(String key, T value) {
            this.character = key.charAt(0);

            if (key.length() > 1) {
                this.mid = new Node<>(key, value, 1);
            } else {
                this.value = value;
            }
        }

        private Node(String key, T value, int index) {
            this.character = key.charAt(index);

            if (index + 1 < key.length()) {
                this.mid = new Node<>(key, value, index + 1);
            } else {
                this.value = value;
            }
        }

        public void search(String query, List<Node<T>> results, int index) {
            if (query == null || query.isEmpty()) {
                this.findAllValues(results);
                return;
            }

            Character c = query.charAt(index);
            if (c.equals(this.character)) {
                if (index == query.length() - 1) {
                    this.findAllValues(results);
                } else {
                    if (this.mid != null) {
                        this.mid.search(query, results, index + 1);
                    }
                }
            } else if (this.character.compareTo(c) > 0) {
                if (this.left != null) this.left.search(query, results, index);
            } else {
                if (this.right != null) this.right.search(query, results, index);
            }
        }

        public void put(String key, T value) {
            Character c = key.charAt(0);
            if (this.character.equals(c)) {

                if (key.length() == 1) {
                    this.value = value;
                } else if (this.mid != null) {
                    this.mid.put(key.substring(1), value);
                } else {
                    this.mid = new Node<>(key.substring(1), value);
                }
            } else if (this.character.compareTo(c) > 0) {

                if (this.left != null) {
                    this.left.put(key, value);
                } else {
                    this.left = new Node<>(key, value);
                }
            } else {

                if (this.right != null) {
                    this.right.put(key, value);
                } else {
                    this.right = new Node<>(key, value);
                }
            }
        }

        private boolean hasValue() {
            return this.value != null;
        }

        private void findAllValues(List<Node<T>> results) {
            if (this.hasValue()) results.add(this);
            if (this.mid != null) this.mid.findAllValues(results);
            if (this.right != null) this.right.findAllValues(results);
            if (this.left != null) this.left.findAllValues(results);
        }

    }
}

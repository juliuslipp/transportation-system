package ie.trinity.persistence;

import ie.trinity.common.model.EdgeEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Edges {
    private final HashMap<Integer, List<EdgeEntry>> edges = new HashMap<>();

    public List<EdgeEntry> getAdjacentEdgeEntries(Integer stopId) {
        List<EdgeEntry> result = this.edges.get(stopId);
        return result == null ? new ArrayList<>() : result;
    }

    public void addEdge(Integer fromStopId, Integer toStopId, Double cost) {
        EdgeEntry newEntry = new EdgeEntry(toStopId, cost);

        if (edges.containsKey(fromStopId)) {
            if (!edges.get(fromStopId).contains(newEntry))
                edges.get(fromStopId).add(newEntry);
        } else {
            ArrayList<EdgeEntry> entryList = new ArrayList<>();
            entryList.add(newEntry);
            edges.put(fromStopId, entryList);
        }
    }
}

package ie.trinity.persistence;

import ie.trinity.common.model.Stop;
import ie.trinity.persistence.data_structures.TernarySearchTree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Stops {
    private final Map<Integer, Stop> stops = new HashMap<>();

    private final TernarySearchTree<Stop> stopNameSearchTree = new TernarySearchTree<>();

    public void addStop(Stop stop) {
        this.stops.put(stop.getId(), stop);
        this.stopNameSearchTree.put(stop.getName(), stop);
    }

    public Stop getStop(Integer stopId) {
        return this.stops.get(stopId);
    }

    public List<Stop> searchStopsByName(String stopName) {
        return this.stopNameSearchTree.search(stopName);
    }
}

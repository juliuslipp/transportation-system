package ie.trinity.controller;

import ie.trinity.business.service.api.DataService;
import ie.trinity.business.service.api.ShortestPathService;
import ie.trinity.common.model.Stop;
import ie.trinity.common.model.StopTime;
import ie.trinity.loader.DataLoader;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;


@RestController
@RequiredArgsConstructor
public class PageController {

    private final DataService dataService;
    private final DataLoader dataLoader;
    private final ShortestPathService shortestPath;

    @GetMapping("/")
    public RedirectView home() {
        return new RedirectView("/pages/overview");
    }

    @GetMapping("/pages/overview")
    public ModelAndView overview() {
        if (!this.dataLoader.isDataLoaded()) return this.getLoadingView();
        return this.getOverview();
    }

    @PostMapping("/pages/shortestPath")
    public ModelAndView shortestPath(@RequestParam(required = false) Integer startId, @RequestParam(required = false) Integer endId) {
        if (!this.dataLoader.isDataLoaded()) return this.getLoadingView();
        Stop startStop = this.dataService.getStops().getStop(startId);
        Stop endStop = this.dataService.getStops().getStop(endId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("shortest-path.html");

        if (startStop == null) {
            modelAndView.getModel().put("error", String.format("The Stop %d does not exist!", startId));
            return modelAndView;
        } else if (endStop == null) {
            modelAndView.getModel().put("error", String.format("The Stop %d does not exist!", endId));
            return modelAndView;
        }

        Map.Entry<Double, List<Stop>> shortestPath = this.shortestPath.calculateShortestPath(startStop, endStop);
        if (shortestPath == null) {
            modelAndView.getModel().put("error", "There is no path between the nodes!");
            return modelAndView;
        }

        modelAndView.getModel().put("shortestPath", shortestPath.getValue());
        modelAndView.getModel().put("cost", shortestPath.getKey());
        return modelAndView;
    }

    @PostMapping("/pages/stops")
    public ModelAndView findStopsByName(@RequestParam String name) {
        if (!this.dataLoader.isDataLoaded()) return this.getLoadingView();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("stops.html");
        List<Stop> stops = this.dataService.getStops().searchStopsByName(name);
        modelAndView.getModel().put("stops", stops);
        modelAndView.getModel().put("isEmpty", stops.isEmpty());
        modelAndView.getModel().put("name", name);
        return modelAndView;
    }

    @PostMapping("/pages/trips")
    public ModelAndView findTripsByTime(@RequestParam String time) {
        if (!this.dataLoader.isDataLoaded()) return this.getLoadingView();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("trips.html");
        List<StopTime> trips = this.dataService.getStopTimes().get(LocalTime.parse(time));
        modelAndView.getModel().put("trips", trips);
        modelAndView.getModel().put("isEmpty", trips.isEmpty());
        modelAndView.getModel().put("time", time);
        return modelAndView;
    }

    private ModelAndView getLoadingView() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("loading.html");
        modelAndView.getModel().put("progress", this.dataLoader.getProgress());
        modelAndView.getModel().put("seconds", String.format("%.2f", this.dataLoader.getEstimatedSeconds()));
        modelAndView.getModel().put("currentFile", this.dataLoader.getCurrentFileLoading());
        modelAndView.getModel().put("totalFiles", DataLoader.TOTAL_FILE_COUNT);
        modelAndView.getModel().put("isLoading", !this.dataLoader.isDataLoaded());
        return modelAndView;
    }

    private ModelAndView getOverview() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("overview.html");
        return modelAndView;
    }
}

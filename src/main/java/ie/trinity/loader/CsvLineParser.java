package ie.trinity.loader;

import ie.trinity.common.model.Stop;
import ie.trinity.common.model.StopTime;
import ie.trinity.common.model.Transfer;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

public final class CsvLineParser {
    private static final String DELIMITER = ",";
    private static final String[] STOP_NAME_PREFIX = new String[]{"wb", "nb", "sb", "eb"};

    public static Stop parseStopEntry(String line) {
        String[] stopArr = trimArray(line.split(DELIMITER));
        Stop stop = new Stop();
        stop.setId(Integer.parseInt(stopArr[0]));
        stop.setCode(stopArr[1].trim().isEmpty() ? null : Integer.parseInt(stopArr[1]));
        stop.setName(CsvLineParser.prepareStopName(stopArr[2]));
        stop.setDescription(stopArr[3]);
        stop.setLat(Double.parseDouble(stopArr[4]));
        stop.setLon(Double.parseDouble(stopArr[5]));
        stop.setZoneId(stopArr[6]);
        stop.setUrl(stopArr[7]);
        stop.setLocationType(stopArr[8]);
        stop.setParentStation(stopArr.length == 10 ? stopArr[9] : null);

        return stop;
    }

    public static StopTime parseStopTimeEntry(String line) {
        String[] stopTimeArr = trimArray(line.split(DELIMITER));
        StopTime stopTime = new StopTime();

        try {
            if (stopTimeArr[1].length() < 8) stopTimeArr[1] = "0" + stopTimeArr[1];
            if (stopTimeArr[2].length() < 8) stopTimeArr[2] = "0" + stopTimeArr[2];

            stopTime.setArrivalTime(LocalTime.parse(stopTimeArr[1]));
            stopTime.setDepartureTime(LocalTime.parse(stopTimeArr[2]));
        } catch (DateTimeParseException e) {
            System.out.printf("%s or %s is/are not valid time formats. Skipping Entry.%n", stopTimeArr[1], stopTimeArr[2]);
            return null;
        }

        stopTime.setTripId(Integer.parseInt(stopTimeArr[0]));
        stopTime.setArrivalTime(LocalTime.parse(stopTimeArr[1]));
        stopTime.setDepartureTime(LocalTime.parse(stopTimeArr[2]));
        stopTime.setStopId(Integer.parseInt(stopTimeArr[3]));
        stopTime.setStopSequence(Integer.parseInt(stopTimeArr[4]));
        stopTime.setHeadSign(stopTimeArr[5]);
        stopTime.setPickUpType(Short.parseShort(stopTimeArr[6]));
        stopTime.setDropOffType(Short.parseShort(stopTimeArr[7]));
        if (stopTimeArr.length == 9) {
            stopTime.setShapeDistanceTraveled(Double.parseDouble(stopTimeArr[8]));
        }

        return stopTime;
    }

    public static Transfer parserTransferEntry(String line) {
        String[] transferArr = trimArray(line.split(DELIMITER));
        Transfer transfer = new Transfer();

        transfer.setFromStopId(Integer.parseInt(transferArr[0]));
        transfer.setToStopId(Integer.parseInt(transferArr[1]));
        transfer.setTransferType(Short.parseShort(transferArr[2]));
        if (transferArr.length == 4) {
            transfer.setMinTransferTime(Integer.parseInt(transferArr[3]));
        }

        return transfer;
    }

    private static String prepareStopName(String stopName) {
        String lowerCase = stopName.toLowerCase();
        for (String prefix : STOP_NAME_PREFIX) {
            if (lowerCase.startsWith(prefix)) {
                return (stopName.substring(prefix.length()) + " " + stopName.substring(0, prefix.length())).trim();
            }
        }
        return stopName.trim();
    }

    private static String[] trimArray(String[] arr) {
        return Arrays.stream(arr).map(String::trim).toArray(String[]::new);
    }
}

package ie.trinity.loader;


import ie.trinity.business.service.DataServiceImpl;
import ie.trinity.common.model.StopTime;
import ie.trinity.common.model.Transfer;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationRunner {
    public static final int TOTAL_FILE_COUNT = 3;

    private static final String STOPS_FILE_PATH = "classpath:input_files/stops.csv";
    private static final String TRANSFER_FILE_PATH = "classpath:input_files/transfers.csv";
    private static final String STOP_TIMES_FILE_PATH = "classpath:input_files/stop_times.csv";

    private final DataServiceImpl dataService;
    private int currentFileLoading = 0;
    private int totalLineCount = 0;
    private int currentLoadedLines = 0;
    private LocalTime loadingStart = LocalTime.now();
    private boolean isLoaded = false;

    private static List<String> readLines(File file) throws IOException {
        List<String> lines = FileUtils.readLines(file, "UTF-8");
        lines.remove(0);
        return lines;
    }

    public double getEstimatedSeconds() {
        long secondsBetween = SECONDS.between(this.loadingStart, LocalTime.now());
        if (this.currentLoadedLines == 0) this.currentLoadedLines = 1;
        return (secondsBetween / (double) currentLoadedLines) * (totalLineCount - currentLoadedLines);
    }

    public boolean isDataLoaded() {
        return this.isLoaded;
    }

    public double getProgress() {
        return this.totalLineCount > 0 ? this.currentLoadedLines / (double) this.totalLineCount : 0.001;
    }

    public int getCurrentFileLoading() {
        return this.currentFileLoading;
    }

    @Override
    public void run(ApplicationArguments args) {
        this.loadingStart = LocalTime.now();
        try {
            System.out.println("Read Stops.");
            this.currentFileLoading = 1;
            this.readStopsFile(ResourceUtils.getFile(STOPS_FILE_PATH));

            System.out.println("Read Transfers.");
            this.currentFileLoading = 2;
            this.readTransfersFile(ResourceUtils.getFile(TRANSFER_FILE_PATH));

            System.out.println("Read StopTimes.");
            this.currentFileLoading = 3;
            this.readStopTimesFile(ResourceUtils.getFile(STOP_TIMES_FILE_PATH));
            this.isLoaded = true;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void readStopsFile(File file) throws IOException {
        final List<String> stopLines = readLines(file);
        this.totalLineCount += stopLines.size();

        for (String line : stopLines) {
            this.dataService.getStops().addStop(CsvLineParser.parseStopEntry(line));
            this.currentLoadedLines++;
        }
    }

    private void readTransfersFile(File file) throws IOException {
        final List<String> transferLines = readLines(file);
        this.totalLineCount += transferLines.size();

        for (String line : transferLines) {
            Transfer transfer = CsvLineParser.parserTransferEntry(line);
            this.addEdge(transfer.getFromStopId(), transfer.getToStopId(), transfer.getCost());
            this.currentLoadedLines++;
        }
    }

    private void readStopTimesFile(File file) throws IOException {
        final List<String> stopTimeLines = readLines(file);
        this.totalLineCount += stopTimeLines.size();

        StopTime prev = null;
        for (String line : stopTimeLines) {
            StopTime stopTime = CsvLineParser.parseStopTimeEntry(line);
            if (stopTime != null) {
                this.dataService.getStopTimes().put(stopTime.getArrivalTime(), stopTime);
                if (prev != null && prev.getTripId().equals(stopTime.getTripId())) {
                    this.addEdge(prev.getStopId(), stopTime.getStopId(), prev.getCost());
                }
                prev = stopTime;
            }
            this.currentLoadedLines++;
        }
    }

    private void addEdge(Integer from, Integer to, double cost) {
        this.dataService.getEdges().addEdge(from, to, cost);
    }
}

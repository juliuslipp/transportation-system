package ie.trinity.business.service;

import ie.trinity.business.service.api.DataService;
import ie.trinity.persistence.Edges;
import ie.trinity.persistence.StopTimes;
import ie.trinity.persistence.Stops;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Getter
public class DataServiceImpl implements DataService {
    private final Edges edges = new Edges();
    private final StopTimes StopTimes = new StopTimes();
    private final Stops stops = new Stops();
}

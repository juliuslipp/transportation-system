package ie.trinity.business.service;

import ie.trinity.business.service.api.ShortestPathService;
import ie.trinity.common.model.EdgeEntry;
import ie.trinity.common.model.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@RequiredArgsConstructor
@Service
public class ShortestPathServiceImpl implements ShortestPathService {
    private final DataServiceImpl dataService;

    @Override
    public Map.Entry<Double, List<Stop>> calculateShortestPath(Stop startStop, Stop endStop) {
        Map<Stop, AStarStop> allStops = new HashMap<>();
        PriorityQueue<AStarStop> toVisitStops = new PriorityQueue<>();

        AStarStop start = new AStarStop(startStop, null, 0., startStop.calculateDistance(endStop));
        toVisitStops.add(start);

        while (!toVisitStops.isEmpty()) {
            AStarStop nextStop = toVisitStops.poll();
            allStops.put(nextStop.stop, nextStop);
            if (nextStop.stop.equals(endStop)) {
                List<Stop> shortestPath = new ArrayList<>();
                double totalCost = nextStop.privateCost;
                AStarStop currentStop = nextStop;
                while (currentStop != null) {
                    shortestPath.add(currentStop.stop);
                    currentStop = allStops.get(currentStop.previousStop);
                }
                Collections.reverse(shortestPath);
                return new AbstractMap.SimpleEntry<>(totalCost, shortestPath);
            }

            for (EdgeEntry edge : this.dataService.getEdges().getAdjacentEdgeEntries(nextStop.stop.getId())) {
                if (nextStop.stop.getId().equals(edge.getToStopId())) continue;
                Stop stop = this.dataService.getStops().getStop(edge.getToStopId());
                AStarStop aStarStop = allStops.getOrDefault(stop, new AStarStop(stop));

                double newCost = nextStop.cost + this.computeCost(nextStop.stop, endStop, edge);
                if (newCost < aStarStop.cost) {
                    aStarStop.previousStop = nextStop.stop;
                    aStarStop.cost = newCost;
                    aStarStop.privateCost = edge.getCost() + nextStop.privateCost;
                    aStarStop.estimatedCost = newCost + stop.calculateDistance(endStop);
                    toVisitStops.add(aStarStop);
                }
            }
        }

        return null;
    }

    private double computeCost(Stop nextStop, Stop endStop, EdgeEntry edge) {
        return nextStop.calculateDistance(endStop) + edge.getCost();
    }

    private static class AStarStop implements Comparable<AStarStop> {
        private final Stop stop;
        public Double privateCost = 0.;
        private Stop previousStop;
        private double cost;
        private double estimatedCost;

        public AStarStop(Stop stop) {
            this(stop, null, Double.MAX_VALUE, Double.MAX_VALUE);
        }

        public AStarStop(Stop stop, Stop previousStop, Double cost, Double estimatedCost) {
            this.stop = stop;
            this.previousStop = previousStop;
            this.cost = cost;
            this.estimatedCost = estimatedCost;
        }

        @Override
        public int compareTo(AStarStop other) {
            return Double.compare(this.estimatedCost, other.estimatedCost);
        }
    }
}

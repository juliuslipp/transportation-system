package ie.trinity.business.service.api;

import ie.trinity.persistence.Edges;
import ie.trinity.persistence.StopTimes;
import ie.trinity.persistence.Stops;

public interface DataService {

    Stops getStops();

    StopTimes getStopTimes();

    Edges getEdges();
}

package ie.trinity.business.service.api;

import ie.trinity.common.model.Stop;

import java.util.List;
import java.util.Map;

public interface ShortestPathService {

    /**
     * Calculates the shortest path between two stops.
     *
     * @param startStop stop to start from.
     * @param endStop   target stop.
     * @return Cost, ShortestPath (list of stops)
     */
    Map.Entry<Double, List<Stop>> calculateShortestPath(Stop startStop, Stop endStop);
}
